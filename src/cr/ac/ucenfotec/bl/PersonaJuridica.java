package cr.ac.ucenfotec.bl;

public class PersonaJuridica extends Persona{

    private String representante;
    private String industria;

    public PersonaJuridica() {
    }

    public PersonaJuridica(String nombre, String identificacion, String direccion, String telefono, String representante, String industria) {
        super(nombre, identificacion, direccion, telefono);
        this.representante = representante;
        this.industria = industria;
    }

    public String getRepresentante() {
        return representante;
    }

    public void setRepresentante(String representante) {
        this.representante = representante;
    }

    public String getIndustria() {
        return industria;
    }

    public void setIndustria(String industria) {
        this.industria = industria;
    }

    @Override
    public String toString() {
        return "representante='" + representante + ", industria='" + industria + super.toString();
    }
}
