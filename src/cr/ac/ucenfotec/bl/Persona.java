package cr.ac.ucenfotec.bl;

public class Persona {

    private String nombre;
    private String identificacion;
    private String direccion;
    private String telefono;

    public Persona() {
    }

    public Persona(String nombre, String identificacion, String direccion, String telefono) {
        this.nombre = nombre;
        this.identificacion = identificacion;
        this.direccion = direccion;
        this.telefono = telefono;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificácio(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public String toString() {
        return "nombre='" + nombre + ", identificácio='" + identificacion + ", direccion='" + direccion + ", telefono='" + telefono;
    }
}
