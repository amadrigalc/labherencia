package cr.ac.ucenfotec.bl;

import java.time.LocalDate;
import java.util.ArrayList;

public class CL {

    private ArrayList<PersonaFisica> listaFisica;
    private ArrayList<PersonaJuridica> listaJuridica;
    private ArrayList<Persona> listaPersonasGeneral;

    public String crearPersonaFisica(String nombre, String identificacion, String direccion, String telefono, String apellido, char genero, int edad, LocalDate fechaNacimiento){
        PersonaFisica persona = new PersonaFisica(nombre,identificacion,direccion,telefono,apellido,genero,edad,fechaNacimiento);
        listaFisica.add(persona);
        listaPersonasGeneral.add(persona);
        return "La persona ha sido ingresada con exito";
    }

    public String crearPersonaJuridica(String nombre, String identificacion, String direccion, String telefono, String representante, String industria){
        PersonaJuridica persona = new PersonaJuridica(nombre,identificacion,direccion,telefono,representante,industria);
        listaJuridica.add(persona);
        listaPersonasGeneral.add(persona);
        return "La persona ha sido ingresada con exito";
    }

    public ArrayList<String> listarPersonaFisica(){
        ArrayList lista =new ArrayList();
        for (PersonaFisica carreraTemp:listaFisica) {
            lista.add(carreraTemp.toString());
        }
        return lista;
    }

    public ArrayList<String> listarPersonaJuridica(){
        ArrayList lista =new ArrayList();
        for (PersonaJuridica carreraTemp:listaJuridica) {
            lista.add(carreraTemp.toString());
        }
        return lista;
    }

    public Persona buscarPersona (String identificacion){
        for (Persona cursoTemp:listaPersonasGeneral) {
            if(cursoTemp.getIdentificacion().equals(identificacion)){
                return cursoTemp;
            }
        }
        return null;
    }




}
