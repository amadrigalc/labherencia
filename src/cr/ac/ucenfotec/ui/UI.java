package cr.ac.ucenfotec.ui;

import com.sun.jdi.CharType;
import cr.ac.ucenfotec.bl.CL;
import cr.ac.ucenfotec.bl.Persona;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDate;

public class UI {

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    private static CL gestor = new CL();

    public static void main(String[] args) throws IOException {
        menu();
    }

    private static void menu() throws IOException {

        int opcion = -1;

        do{
        System.out.println("***********Herencia*********");
        System.out.println("1. Ingresar una persona Fisica");
        System.out.println("2. Ingresar una persona Juridica");
            System.out.println("3. Imprimir las personas fisicas");
            System.out.println("4. Imprimir las personas Juridicas");
            System.out.println("5. Buscar persona por identificación");
        System.out.println("0. Salir");
        System.out.print("Digite la opción a procesar: ");
        opcion = Integer.parseInt(in.readLine());

        procesarOpcion(opcion);}
        while (opcion != 0);
    }

    private static void procesarOpcion(int opcion) throws IOException {

        switch (opcion){
            case 1:
                registrarPersonaFisica();
                break;
            case 2:
                registrarPersonaJuridica();
                break;
            case 3:
                imprimirFisica();
                break;
            case 4:
                imprimirJuridica();
                break;
            case 5:
                buscarPersona();
            case 0:
                System.out.println("Gracias por su visita");
                break;
            default:
                System.out.println("Opción invalidad");
        }
    }

    private static void registrarPersonaFisica() throws IOException {
        System.out.print("Digite el nombre de pila de la persona: ");
        String nombre = in.readLine();
        System.out.print("Digite el apellido: ");
        String apellido = in.readLine();
        System.out.print("Digite el número de indentificación: ");
        String identificacion = in.readLine();
        System.out.print("Digite la dirección de la persona: ");
        String direccion = in.readLine();
        System.out.print("Digite el número de telefono: ");
        String telefono = in.readLine();
        System.out.print("Ingrese el genero de la persona: ");
        String generoString = in.readLine();
        char genero = generoString.charAt(0);
        System.out.print("Ingrese la edad");
        int edad = Integer.parseInt(in.readLine());
        System.out.print("Ingrese la fecha de nacimiento por año, mes, dia: ");
        int anho = Integer.parseInt(keyboard.nextline());
        String fecha = in.readLine();
        LocalDate fechaNacimiento = LocalDate.parse(fecha);
////        String fecha = "2009-06-29";
////        LocalDate fechaNacimiento = LocalDate.parse(fecha);
//        System.out.println(fechaNacimiento);

        String respuesta = gestor.crearPersonaFisica(nombre,identificacion,direccion,telefono,apellido,genero,edad,fechaNacimiento);
        System.out.println(respuesta);

    }

    private static void registrarPersonaJuridica() throws IOException {
        System.out.print("Digite el nombre de pila de la persona: ");
        String nombre = in.readLine();
        System.out.print("Digite el número de indentificación: ");
        String identificacion = in.readLine();
        System.out.print("Digite la dirección de la persona: ");
        String direccion = in.readLine();
        System.out.print("Digite el número de telefono: ");
        String telefono = in.readLine();
        System.out.print("Ingrese el representante");
        String representante = in.readLine();
        System.out.print("Ingrese la industria a la que se dedica");
        String industria = in.readLine();

        String respuesta = gestor.crearPersonaJuridica(nombre,identificacion,direccion,telefono,representante,industria);
        System.out.println(respuesta);
    }

    private static void imprimirFisica() {
        System.out.println("*** Listado de personas fisicas ***");
        for (String cursoTemp :gestor.listarPersonaFisica()) {
            System.out.println(cursoTemp);
        }
    }

    private static void imprimirJuridica() {
        System.out.println("*** Listado de personas Juridicas***");
        for (String cursoTemp :gestor.listarPersonaJuridica()) {
            System.out.println(cursoTemp);
        }
    }

    private static void buscarPersona() throws IOException {
        System.out.println("Ingrese el número de identificación a buscar:");
        String identificacion = in.readLine();
        Persona personaABuscar = gestor.buscarPersona(identificacion);
        System.out.println(personaABuscar.toString());
    }


}
